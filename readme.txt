项目中代码解读：
	项目中,xml配置与包名后缀名对应着（大部分对应）。 比如 beans-jdbc  --> com.hp.spring.jdbc

1. helloworld 对应了 beans.xml
	这里是spring的一个helloworld的代码。

2. ref 也对应了  bean-auto.xml
	其中,这里描述各种配置方式：如 ref注入其他对象,自动注入 ,SpEL表示式,使用带参构造器，工厂方式等。

3. annotation 对应了 beans-annotation.xml 
	表示注解的方式，其中有一个例子，是spring4的泛型注入
	
4. aop包下： 分为3个子包：对应的配置文件为：beans-aspectj.xml 和beans-aspectj-xml.xml
	proxy : 		使用jdk代理的例子
	annotation :	spring使用 aspectj 注解的方式的aop
	annotation :	spring使用 xml的aop
	
5. jdbc包：  表结构jdbc.properties   jdbc的例子 

6. tx包(spring中的事物)
	annotation ： 注解方式的使用事物
	xml  : xml方式的使用事物:  
		都用了tx.properties 的数据库接口