package com.hp.spring.tx.annotation;

public interface BookShopService {
	
	public void purchase(String username, String isbn);
	
}
