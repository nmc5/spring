drop database if exists spring;
create database spring;
use spring;

drop table if exists employees;
drop table if exists departments;

create table departments(
	id int(11) primary key auto_increment, 
	dept_name varchar(20)
);

create table employees(
  id int(11) primary key auto_increment, 
	last_name varchar(20),
	email varchar(20),
	dept_id int(11),
	constraint `FK_employees_dept_id` foreign key (dept_id) references departments (id) 
);